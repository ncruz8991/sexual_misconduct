/************ Colors ************/

var red = '#FF5252';
var darkred = '#D50000';
var offwhite =  '#ECF0F1';
var lightblue =  '#3498DB';
var mediumblue =  '#2980B9';
var darkblue =  '#2C3E50';

var mColors = new Array(6);
mColors[0] = red;
mColors[1] = darkred;
mColors[2] = offwhite;
mColors[3] = lightblue;
mColors[4] = mediumblue;
mColors[5] = darkblue;

var gColors = [ '#EA80FC', '#E040FB', '#448AFF', '#2962FF' ];

/************ Global stuff ************/

Highcharts.setOptions({
    chart: {
        style: {
            fontFamily: 'Roboto'
        }
    }
});

function setColor(color) {
    return color;
}

/************ Main charts ************/

// Circle SVGS for chart3 (odds ratios)
female_male = [ 200, 72  ];
ug_gp       = [ 200, 112 ];
lgbt_hetero = [ 200, 124 ];
greek_ged   = [ 200, 126 ];
club        = [ 200, 145 ];

// Bar chart for Tables 4D.1 - 4D.4, 4D.2-4, 4D.1-4
var categories_4D = [
    'Verbal pressuring',
    'Taking advantage of victim<br/>while drunk/on drugs',
    'Taking advantage of victim<br/>while unconscious/asleep',
    'Threatening physical harm',
    'Using physical force',
    'Yes to any above'
];
var categories_4D_full = [
    "verbally pressured after they said they didn't want to into ", // some form of sexual assault
    "taken advantage of while under the influence of drugs or were too drunk to stop ",
    "taken advantage of while unconscous or asleep or physically incapacitated and could not stop ",
    "physically threatened into ",
    "forced, such as being held down, having their arms pinned, or having used a weapon against them into ",
    "verbally pressured, taken advantage of while unconscious, asleep, or under the influence, physically threatened, or forced into "
];
var type_endings = [
    "getting fondled, kissed, or rubbed up against without consent.",
    "giving or getting non-consensual oral sex.",
    "giving or getting non-consensual vaginal sex.",  
    "giving or getting non-consensual anal sex.",
    "giving or getting non-consensual oral, vaginal, or anal sex.",
    "some form of sexual assault."
];

/*
    For any datasets related to table 4D, each array within data_4Dn should be
        Female Undergraduates
        Female Graduates
        Male Undergraduates
        Male Graduates
*/
// This is here because highcharts does some messed up stuff to their arrays
var zeros = [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0]
];

var data_4D1 = [
    [10.2,  11.1,  2.6,  0.4,  3.6,  19.5],
    [5.5,    2.9,  1.5,  0.4,  1.5,   9.0],
    [3.2,    3.2,  1.2,  0.7,  0.5,   6.7],
    [0.7,    0.4,    0,    0,    0,   1.1]
];

var data_4D2 = [
    [5.5,  3.6,  0.4,  0.3,  1.1,  7.7],
    [0.4,  0.8,  0.4,  0,    0.4,  0.8],
    [0.9,  0.9,  0.9,  0.3,  0.8,  1.7],
    [0.4,  0,    0,    0,    0,    0.4]
];

var data_4D3 = [
    [4.0, 4.5, 1.1, 0.3, 1.6, 7.2],
    [1.7, 1.2, 0.4, 0, 0.4, 3.3],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0]
];

var data_4D4 = [
    [1.9, 1.0, 0.1, 0.2, 0.3, 2.7],
    [0.2, 0, 0.5, 0, 0, 0.7],
    [0.8, 0.4, 0.6, 0.8, 0.4, 1.4],
    [0, 0, 0, 0, 0, 0]
];

var data_4D2_4 = [
    [8.0, 6.7, 1.1, 0.3, 1.8, 11.9],
    [2.3, 1.5, 1.2, 0, 0.8, 4.6],
    [0.9, 0.9, 0.9, 0.8, 1.1, 2.0],
    [0.4, 0, 0, 0, 0, 0.4]
];

var data_4D1_4 = [
    [13.1, 12.8, 2.9, 0.4, 3.9, 22.5],
    [5.7, 3.2, 2.0, 0.4, 1.5, 9.2],
    [3.9, 3.2, 1.7, 0.9, 1.3, 6.8],
    [0.7, 0.4, 0, 0, 0, 1.1]
];

var sa_tile_text = [
    "Fondling, kissing, rubbing up against, or removal of clothes without consent",
    "Non-consensual oral sex",
    "Non-consensual vaginal sex",
    "Non-consensual anal sex",
    "Non-consensual oral, vaginal, or anal sex",
    "Any"
];

// Returns a full description version of a given category
function get_full_description_chart_4D(cat_in) {
    switch(cat_in) {
        case categories_4D[0]:
            return categories_4D_full[0];
            
        case categories_4D[1]:
            return categories_4D_full[1];
            
        case categories_4D[2]:
            return categories_4D_full[2];
            
        case categories_4D[3]:
            return categories_4D_full[3];
            
        case categories_4D[4]:
            return categories_4D_full[4];
            
        case categories_4D[5]:
            return categories_4D_full[5];
            
        default:
            break;
    }
}

var prev_sa_tile = null;
var selected_data;

$(function () {

    var type_ending = document.getElementById('chart_4D_type_ending');

    function getDataSource_chart_4D(sa_tile_text_in) {        
        switch (sa_tile_text_in) {
            case sa_tile_text[0]:
                type_ending.innerHTML = type_endings[0];
                return data_4D1;
                
            case sa_tile_text[1]:
                type_ending.innerHTML = type_endings[1];
                return data_4D2;
                
            case sa_tile_text[2]:
                type_ending.innerHTML = type_endings[2];
                return data_4D3;
                
            case sa_tile_text[3]:
                type_ending.innerHTML = type_endings[3];  
                return data_4D4;

            case sa_tile_text[4]:
                type_ending.innerHTML = type_endings[4];
                return data_4D2_4;

            case sa_tile_text[5]:
                type_ending.innerHTML = type_endings[5];
                return data_4D1_4;

        }
    }
    
    var $window = $(window);
    var didScroll = false;

    $window.on('scroll', function () {
        //detected a scroll event, you want to minimize the code here because this event can be thrown A LOT!
        didScroll = true;
    });

    //check every 250ms if user has scrolled to the skills section
    setInterval(function () {
        if (didScroll) {
            didScroll = false;
            checkScrolls();
        }
    }, 250);

    var early_offset = 100;

    var $female_male_ratio = $("#female_male_ratio"),
        gender_done = false;
    var female_male_waypoint = $female_male_ratio.offset().top + $female_male_ratio.height() - early_offset;

    var $ug_gp_odds_ratio = $("#ug_gp_odds_ratio"),
        grad_done = false;
    var ug_gp_odds_waypoint = $ug_gp_odds_ratio.offset().top + $ug_gp_odds_ratio.height() - early_offset;

    var $lgbt_hetero_ratio = $("#lgbt_hetero_ratio"),
        lgbt_done = false;
    var lgbt_hetero_waypoint = $lgbt_hetero_ratio.offset().top + $lgbt_hetero_ratio.height() - early_offset;
    
    var $greek_ged_ratio = $("#greek_ged_ratio"),
        greek_done = false;
    var greek_ged_waypoint = $greek_ged_ratio.offset().top + $greek_ged_ratio.height() - early_offset;    
    
    var $club_not_club_ratio = $("#club_not_club_ratio"),
        club_done = false;
    var club_not_club_waypoint = $club_not_club_ratio.offset().top + $club_not_club_ratio.height() - early_offset;

    function checkScrolls() {

        var scrollBottom = $window.scrollTop() + $window.height();
        if (!gender_done && (scrollBottom > female_male_waypoint)) {
            $("#svg_female").velocity({ r: 200 });
            $("#svg_male").velocity({ r: 72 });
            gender_done = true;
        }
        if (!grad_done && (scrollBottom > ug_gp_odds_waypoint)) {
            $("#svg_grad").velocity({ r: 112 });
            $("#svg_ug").velocity({ r: 200 });
            grad_done = true;
        }
        if (!lgbt_done && (scrollBottom > lgbt_hetero_waypoint)) {
            $("#svg_lgbt").velocity({ r: 200 });
            $("#svg_hetero").velocity({ r: 124 });
            lgbt_done = true;
        }
        if (!greek_done && (scrollBottom > greek_ged_waypoint)) {
            $("#svg_greek").velocity({ r: 126 });
            $("#svg_ged").velocity({ r: 200 });
            greek_done = true;
        }
        if (!club_done && (scrollBottom > club_not_club_waypoint)) {
            $("#svg_club").velocity({ r: 200 });
            $("#svg_no_club").velocity({ r: 145 });
            club_done = true;

            $window.off('scroll'); //remove listener that will create chart, this ensures the chart will be created only once
        }
    };
    
    $('#chart_4D').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: "Percent of students that have experienced this type of sexual assault"
        },
        subtitle: {
            text: "organized by method of nonconsensual behavior"
        },
        xAxis: {
            categories: categories_4D,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            max: 25,
            title: {
                text: 'Percentage of students experience the given sexual assault (%)',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: '%'
        },
        plotOptions: {
            bar: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                point: {
                    events: {
                        click: function() {
                            // Store the selected datapoint
                            selected_data = this;
                            
                            // Get rid of the default prompt
                            document.getElementById('chart_4D_default_prompt')
                                    .style.display = 'none';
                            document.getElementById('chart_4D_selected_prompt')
                                    .style.display= 'block';
                            
                            // Set the percentage in the block
                            var percent = document.getElementById('chart_4D_percentage');
                            percent.innerHTML = this.y;
                            percent.style.backgroundColor = this.color;
                            percent.style.color = '#FFF';
                            
                            // Set the series name
                            var people = document.getElementById('chart_4D_series');
                            people.innerHTML = this.series.name.toLowerCase();
                            people.style.color = this.color;
                            
                            // Set the category description
                            var cat_desc = document.getElementById('chart_4D_category_description');
                            cat_desc.innerHTML = get_full_description_chart_4D(this.category);
                            cat_desc.style.color = this.color;

                            // Set the type color
                            document.getElementById('chart_4D_type_ending').style.color = this.color;
                        }
                    }
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            reversed: true
        },
        credits: {
            href: 'https://publicaffairs.vpcomm.umich.edu/wp-content/uploads/sites/19/2015/04/Complete-survey-results.pdf',
            text: 'Source: umich.edu',
        },
        series: [{
            name: 'Male Graduates',
            data: zeros[3],
            color: mColors[4]
        }, {
            name: 'Male Undergraduates',
            data: zeros[2],
            color: mColors[3]
        }, {
            name: 'Female Graduates',
            data: zeros[1],
            color: mColors[1]
        }, {
            name: 'Female Undergraduates',
            data: zeros[0],
            color: mColors[0]
        }]
    });
    
    // Load tiles
    $('#sa_type_4D1').text(sa_tile_text[0]);
    $('#sa_type_4D2').text(sa_tile_text[1]);
    $('#sa_type_4D3').text(sa_tile_text[2]);
    $('#sa_type_4D4').text(sa_tile_text[3]);
    $('#sa_type_4D2-4').text(sa_tile_text[4]);
    $('#sa_type_4D1-4').text(sa_tile_text[5]);
    
    // On sa_tile click, load the other one
    $(".sa_tile").click(function() {
        $('#chart_4D_selected_prompt').css("display", "none");
        $('#chart_4D_default_prompt').css("display", "");
        
        if (prev_sa_tile !== null) {
            prev_sa_tile.style.backgroundColor = '';
            prev_sa_tile.style.color = '';
        }
        this.style.backgroundColor = mediumblue;
        this.style.color = '#FFF';
        prev_sa_tile = this;
        
        // Grab the data related to the type of sexual assault
        var chart_4D = $('#chart_4D').highcharts();
        var new_data = getDataSource_chart_4D(this.innerHTML);
        chart_4D.series[0].setData(new_data[3], false);
        chart_4D.series[1].setData(new_data[2], false);
        chart_4D.series[2].setData(new_data[1], false);
        chart_4D.series[3].setData(new_data[0], false);
        chart_4D.redraw();
        
        console.log(new_data[0]);
        selected_data.select(false);
    });
    
    $("#sa_type_4D1").trigger("click");
});